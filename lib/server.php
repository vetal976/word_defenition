<?php 
function getWordsArr(){
    $words_arr = array();
    $handle = fopen("../data/words_en.csv", "r");
    while (($data = fgetcsv($handle, 200, ";")) !== FALSE) {
        array_splice($data, 2, 1);
        $words_arr[array_shift($data)] = $data[0];
    }
    fclose($handle);
    return $words_arr;
}

function getLessonsArr(){
    $lesson_arr = array();
    $handle = fopen("../data/lessons.csv", "r");
    while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
        array_shift($data);
        array_splice($data, 2, 1);    
        $lesson_arr[array_shift($data)] = $data;
    }
    fclose($handle);
    return $lesson_arr;
}

$response = array("lessons" =>getLessonsArr(), "words"=>getWordsArr() );
$response = json_encode($response);
header('Content-Type: application/json');
print $response;

